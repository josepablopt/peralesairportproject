"""
Jose Perales
Programming Languages.

peralesairportProject/src/main.py

Airport take-off time slot simulation in Python.
In this project you will use the Python programming language to simulate aircraft take off time slots at an airport.
You need to keep track of the information needed to schedule the airstrip resource: request identifier, request submission time, time slot requested, length of time requested, actual start time, actual end time. You should do this by creating a class to hold all this info.
You need to have a queue of airplanes waiting before they can take off. You should be able to print out the status of the queue as time moves along. You may do this by using a built-in data structure or by creating a class.
You should be able to load the requests from a file. For example, if the input file looked like this:

ID, Submission Time, Requested Start, Requested Duration
Delta 160, 0, 0, 4
UAL 120, 0, 5, 4
Delta 6, 2, 3, 6

At time 0 the queue would look like: Delta 160 (started at 0), UAL 120 (scheduled for 5)
At time 1 the queue would look like: Delta 160 (started at 0), UAL 120 (scheduled for 5)
At time 2 the queue would look like: Delta 160 (started at 0), Delta 6 (scheduled for 4), UAL 120 (scheduled for 10)
At time 3 the queue would look like: Delta 160 (started at 0), Delta 6 (scheduled for 4), UAL 120 (scheduled for 10)
At time 4 the queue would look like: Delta 6 (started at 4), UAL 120 (scheduled for 10)
...

At the end, you should print out a listing of the actual take off times:

Delta 160 (0-3), Delta 6 (4-9), UAL 120 (10-13)

Please remember that you must have at least two branches (working and master) in your git repository. The only commits on the master branch should be tagged and correspond to the three stages of this project.
"""


#Class is creating a RequestPorcess object using the input file and calling the run method:


import sys
import RequestProcess as RequestProcess


if __name__ == "__main__":
    rp = RequestProcess.RequestProcess(sys.argv[1])
    rp.run()






