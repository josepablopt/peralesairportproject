"""
Jose Perales
peralesairportProject/src/RequestProcess


"""

##Readind the file that ends in .txt or .csv

import heapq
import time


class RequestProcess:
    def __init__(self, file_name):
        self.__input_list = []
        self.__file_name = file_name

    ##Handinding the input of the files
    ##Checking to see if the file ends is valid. If not then it sends the user a message
    ##While the file is open its splitting the lines and validating that its the right format.
    ##If the file is the right format it returns true to the run function.
    def __process_input(self):
        if self.__file_name.endswith('.txt'):
            with open(self.__file_name) as input_file:
                whole_file = input_file.read().splitlines()
                for i in range(len(whole_file)):
                    whole_file[i] = whole_file[i].split(',')
                    for j in range(len(whole_file[i])):
                        whole_file[i][j] = whole_file[i][j].strip()
            if self.__validatingTheData(whole_file):
                for item in whole_file:
                    self.__input_list.append(Request(item[0], int(item[1]), int(item[2]), int(item[3])))
                theCurrentQueue = PriorityQueue()
                for i in self.__input_list:
                    theCurrentQueue.push(i, i.getSubmitionTime(), i.getStartTime())
                for i in range(len(self.__input_list)):
                    self.__input_list[i] = theCurrentQueue.pop()
                return True
            else:
                return False
        else:
            print("Please makesure that the input file is a .txt file")
            return False
    ##Validating the data this is being called from the Prcess input function.
    def __validatingTheData(self, dataa):
        for item in dataa:
            if len(item) != 4:
                print(item)
                print("Make sure that there are four 4 values on each of the lines ")
                valid_input = False
                break
            valid_input = True
        return valid_input

    ##checking to see if it meets the requeterments. if so then remove or just return the list,
    @staticmethod
    def __checkRemoved(theCurrentList, timer):
        if len(theCurrentList) > 0 and theCurrentList[0].getActualEnd() <= timer:
            theCurrentList.remove(theCurrentList[0])
        return theCurrentList

    ##Taking in a list and retunring a sorted object.
    @staticmethod
    def __sortByRequestedTime(passesList):
        queueOne = PriorityQueue()
        for i in passesList:
            queueOne.push(i, i.getStartTime(), i.getSubmitionTime())
        for i in range(len(passesList)):
            passesList[i] = queueOne.pop()
        return passesList

    ##Calulating Times
    @staticmethod
    def __calculateTimes(passedList):
        if len(passedList) > 0:
            passedList[0].settingTheActualStartTime(
                max(int(passedList[0].getActualStart()), int(passedList[0].getStartTime())))
            for i in range(1, len(passedList)):
                offset = passedList[i - 1].getActualStart() + passedList[i - 1].getDuratation()
                if offset > passedList[i].getStartTime():
                    passedList[i].getActualStart(offset)
                else:
                    passedList[i].settingTheActualStartTime(passedList[i].getActualStart())
            for i in range(len(passedList)):
                passedList[i].settingTheActualEndTime(passedList[i].getActualStart() + passedList[i].getDuratation())
        return passedList

    ##Formting the output and taking in the list:
    ##Fufulling one of the requetments.
    def __formattedOut(self, passedList, timer):
        returnValue = "At time " + str(timer) + " the queue would look like: "
        if len(passedList) > 0:
            if len(passedList) == 1:
                if passedList[0].getActualStart() <= timer:
                    returnValue += passedList[0].getID() + " (started at " + str(passedList[0].getActualStart()) + ")"
                else:
                    returnValue += passedList[0].getID() + " (scheduled for " + str(passedList[0].getActualStart()) + ")"
                return returnValue
            else:
                if passedList[0].getActualStart() <= timer:
                    returnValue += passedList[0].getID() + " (started at " + str(passedList[0].getActualStart()) + "), "
                else:
                    returnValue += passedList[0].getID() + " (scheduled for " + str(passedList[0].getActualStart()) + "), "
            for i in range(1, len(passedList) - 1):
                returnValue += passedList[i].getID() + " (scheduled for " + str(passedList[i].getActualStart()) + "), "
            returnValue += passedList[len(passedList) - 1].getID() + " (scheduled for " + str(passedList[len(passedList) - 1].getActualStart()) + ")"
        else:
            returnValue += "Empty list"
        return returnValue

    ##this is the final print line
    @staticmethod
    def __finalPrint(passedList):
        value_tobe_sent_back = ""
        if len(passedList) > 0:
            for i in range(len(passedList) - 1):
                value_tobe_sent_back += passedList[i].getID() + " (" + str(passedList[i].getActualStart()) + "-" + str(
                    passedList[i].getActualEnd()) + "), "
            value_tobe_sent_back += passedList[len(passedList) - 1].getID() + " (" + str(
                passedList[len(passedList) - 1].getActualStart()) + "-" + str(
                passedList[len(passedList) - 1].getActualEnd()) + ")"
        else:
            value_tobe_sent_back += "Empty."
        return value_tobe_sent_back

    ##Brains of the operation. starts by calling the function that is processing the input.
    def run(self):
        if self.__process_input():
            current_list = []
            final_list = []
            timer = 0
            while True:
                current_list = self.__checkRemoved(current_list, timer)
                for i in self.__input_list:
                    if i.getSubmitionTime() == timer:
                        current_list.append(i)
                        final_list.append(i)
                current_list = self.__sortByRequestedTime(current_list)
                current_list = self.__calculateTimes(current_list)
                if len(current_list) == 0 and len(final_list) == len(self.__input_list):
                    break
                print(self.__formattedOut(current_list, timer))
                time.sleep(1)
                timer += 1
            final_list = self.__sortByRequestedTime(final_list)
            print(self.__finalPrint(final_list))


##Definding the priority Queue
class PriorityQueue:
    def __init__(self):
        self.__que = []  # Storing the queue in this list
        self.__index = 0  # the index in queue

    ## the Push will call amnd use the first priority, then second, them finally the index.
    ## the top of the queue is the lowest priorty so it will get poped first.

    def push(self, item, priority, priority2):
        heapq.heappush(self.__que, (priority, priority2, self.__index, item))

    ## heappop is retunring the smallest item in the queue:
    def pop(self):
        return heapq.heappop(self.__que)[-1]

    ##this is going to return true if the queue is emply.

    def empty(self):
        return len(self.__que) == 0


##This class is going to be called by a plane that is requesting to take off. It's going to schedudle take offs.
class Request:
    def __init__(self, req_id, sub_time, req_start, req_duratiation):
        self.__id = str(req_id)  # Plane that is making that request.
        self.__sub_time = int(sub_time)  # Time that the request was made
        self.__req_starts = int(req_start)  # the time that the plane request to start.
        self.__req_duratiaon = int(req_duratiation)  # the time that the plane has requested to take.
        self.__actual_start = 0  # time that the plane actual starts
        self.__actual_end = 0  # time that the plane actual ends.

    ##Going to return a formated
    def __str__(self):
        return "[" + self.__id + ", " + str(self.__sub_time) + ", " + str(self.__req_starts) + ", " + str(
            self.__req_duratiaon) + ", " + str(self.__actual_start) + ", " + str(self.__actual_end) + "]"

    ##Retunring the string
    def __repr__(self):
        return str(self)

    ##Returning the submition time
    def getSubmitionTime(self):
        return self.__sub_time

    ##returing the requested start time
    def getStartTime(self):
        return self.__req_starts

    def getActualEnd(self):
        return self.__actual_end

    def getActualStart(self):
        return self.__actual_start

    ##Returing the duratation
    def getDuratation(self):
        return self.__req_duratiaon

    ##Taking in an int and setting the actual start time
    def settingTheActualStartTime(self, start):
        return self.__actual_start == start

    def settingTheActualEndTime(self, end):
        return self.__actual_end == end

    def getID(self):
        return self.__id
